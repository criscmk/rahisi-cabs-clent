import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  serverUrl: string = 'https://rahisicabs.co.ke/api'
  
  playstoreUrl: string = 'https://play.google.com/store/apps/details?id=com.rahisicabs.co.ke'
  fcmToken: string = ''

  httpOptions: any = {};
  httpOptions2: any = {};

  loginAccessToken: string = ''
  user_details: any = []

  name: string = ''
  email: string = ''

  baseFare: number = 50
  costperKM: number = 10
  costperMinute = 4
  minFare = 50

  startLocation: any = {}
  destination: any = {}
  distance: number = 0
  travelTime: number = 10
  driverName: string = ''
  driverPhoneNumber: string = ''
  driverID: number = 0
  vehicleRegistration: string = ''
  cost: number = 0

  baseFareEco: any = 100;
  minFareEco: any = 150;
  costperKMEco: any = 4;
  costperMinuteEco: any = 3;

  minFareBis: any = 200;
  costperKMBis: any = 20;
  costperMinuteBis: any = 5;
  baseFareBis: any = 200;

  totalFareEco: number;
  totalFareBis: any;
  phone: any;
  connection: boolean;
  currentAppVersion: any="1.0.2";
  recentRides: any=[];
  maptype: number=1;
  radius: any;
  duration: any;

  constructor(public http: HttpClient) {

    // create the set of http headers accepted by the server
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Accept": 'application/json'
      })
    };

  }

  // pass in data and url (using the server url)
  templateLoader(url, data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.serverUrl + url, data, this.httpOptions)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    })
  }

  mainPost(url, data) {
    const reqOpts = {
      headers: {
        'Authorization': 'Bearer ' + this.loginAccessToken,
        'Content-Type': 'application/json',
      },
    };
    console.log("http headers", reqOpts);
    return new Promise((resolve, reject) => {
      this.http.post(this.serverUrl + url, data, reqOpts)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    })
  }

  postNotification(url, data) {
    const reqOpts = {
      headers: {
        'Authorization': 'key=AAAA6450uOI:APA91bFMHvRyJ38LxKOhWPhjrztxw-SU6dd6tBE7Yhma3ZUfSBMxnS0hFCqvHhZT9cEYc6Bc-4A9L2o2YE69xP9acdoIcOpi1svG10EPThGb7n3rzvm9YqjsjxXSW8AN5gZ35mkZJFIM',
        'Content-Type': 'application/json',
      },
    };
    console.log("http headers", reqOpts);
    return new Promise((resolve, reject) => {
      this.http.post(url, data, reqOpts)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    })
  }

  mainGet(url) {

    const reqOpts = {
      headers: {
        'Authorization': 'Bearer ' + this.loginAccessToken,
        'Content-Type': 'application/json',
      },
    };

    console.log("http headers", reqOpts);
    return this.http.get(this.serverUrl + url, reqOpts)
  }


  Get2(url) {
    return this.http.get(this.serverUrl + url)
  }

}
