import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage'
import { RestProvider } from '../providers/rest/rest';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Network } from '@ionic-native/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // rootPage = "HomePage";

  pages: Array<{ title: string, icon: string, component: any }>;
  access_token: any;
  user_details: any;

  constructor(public platform: Platform, private socialSharing: SocialSharing,
    public statusBar: StatusBar, public splashScreen: SplashScreen,
    public storage: Storage, private rest: RestProvider,
    public app: App,
    public network: Network,
    private locationAccuracy: LocationAccuracy, private androidPermissions: AndroidPermissions,
    private loadingCtrl: LoadingController, private alertCtrl: AlertController, ) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', icon: 'ios-home', component: 'HomePage' },
      { title: 'Free ride', icon: 'md-contacts', component: 'FreeRidePage' },
      { title: 'Trip History', icon: 'ios-time', component: 'HistoryPage' },
      // { title: 'Shedule Ride', icon: 'md-calendar', component: 'ScheduleRidePage' },
      { title: 'Account Settings', icon: 'md-settings', component: 'page-my-account' },
      { title: 'Help & Support', icon: 'md-help', component: 'page-support' },
      // { title: 'Logout', icon: 'log-in', component: 'ListPage' }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.checkGPSPermission()
      this.checkConnection();
      // this.handleBackButton();

      this.storage.get('STORAGE_KEY_TOKEN').then(token => {
        this.access_token = token;

        if (!this.access_token) {
          this.nav.setRoot('LoginPage');
        }
        else {
          this.rest.loginAccessToken = token
          this.recentRides();
          console.log("Logged user", JSON.stringify(this.access_token));
          // get the details of the user
          this.storage.get("profile").then(user => {
            console.log("The user data: ", user)
            this.user_details = user;
            this.rest.user_details = user;
            this.rest.name = this.user_details.name;
            this.rest.email = this.user_details.email;
            this.rest.phone = this.user_details.phone;
          
            // this.nav.setRoot('RateModalPage');
            this.nav.setRoot('HomePage')
            

          }, error => {
            console.log("failed to retrieve user details")
            this.nav.setRoot('LoginPage');
          })

        }
      }, error => {
        console.log("Error checking logged user", JSON.stringify(error));
        this.nav.setRoot('LoginPage');
      });

    });
  }

  
  logout() {
    this.nav.setRoot('LoginPage');
    this.nav.setRoot('LoginPage');
    console.log('twende');
    this.storage.clear();
    localStorage.clear();

  }

  //Check if application having GPS access permission  
  checkGPSPermission() {

    let loading = this.loadingCtrl.create({
      content: 'Just a moment...',
      spinner: 'crescent'
    })

    loading.present()

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
          loading.dismiss()
        } else {
          //If not having permission ask for permission
          this.requestGPSPermission();
          loading.dismiss()
        }
      },
      err => {
        loading.dismiss()
        this.simpleAlert(err)
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(() => {
            // call method to turn on GPS
            this.askToTurnOnGPS();
          }, error => {
            //Show alert if user click on 'No Thanks'
            this.simpleAlert('RequestPermission Error requesting location permissions ' + error)
          }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
      // When GPS Turned ON call method to get Accurate location coordinates
      // this.getUserLocation()
      this.storage.get('STORAGE_KEY_TOKEN').then(token => {
        this.access_token = token;

        if (!this.access_token) {
          this.nav.setRoot('LoginPage');
        }
        else {
          this.rest.loginAccessToken = token
          console.log("Logged user", JSON.stringify(this.access_token));

          // get the details of the user
          this.storage.get("profile").then(user => {
            console.log("The user details: ", user)
            this.user_details = user;
            this.rest.user_details = user;
            this.rest.name = this.user_details.name;
            this.rest.email = this.user_details.email;

            // this.nav.setRoot('RateModalPage');
            this.nav.setRoot('HomePage')

          }, error => {
            console.log("failed to retrieve user details", error)
            this.nav.setRoot('LoginPage');
          })

        }
      }, error => {
        console.log("Error checking logged user", JSON.stringify(error));
        this.nav.setRoot('LoginPage');
      });
    }, error => this.simpleAlert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  shareApp() {

    let message = 'Download Rahisi App from play store and get a ride at the click of a button'
    this.socialSharing.share(message, this.rest.playstoreUrl)
      .then(() => {

      })
      .catch((error) => {
        console.log(error)
      });

  }

  simpleAlert(message) {
    this.alertCtrl.create({
      message: message,
      mode: 'ios',
      buttons: [{
        text: 'Dismiss',
        handler: () => {

        }
      }]
    }).present()
  }

  openPage(page) {

    this.nav.setRoot(page);
    console.log('Pages', page);
  }
  open1Page(page) {

    this.nav.setRoot(page.component);

  }

  profile() {
    this.nav.setRoot('ProfilePage');
  }

  goBack() {
    this.nav.setRoot('MenuPage');
  }

  helpAndSupport() {
    this.nav.setRoot('SupportPage')
  }

  //handle back button
  handleBackButton() {
    this.platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();
      // Checks if can go back before show up the alert
      if (activeView.name === 'HomePage') {
        if (nav.canGoBack()) {
          // nav.pop();
          this.platform.exitApp();

        } else {
          const alert = this.alertCtrl.create({
            title: 'Warning',
            mode: 'ios',
            message: 'Are you sure you want to exit the application',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                this.nav.setRoot('HomePage');
                console.log('** Saída do App Cancelada! **');
              }
            }, {
              text: 'Okay',
              handler: () => {
                //this.logout();
                this.platform.exitApp();
              }
            }]
          });
          alert.present();
        }
      }
      else if (activeView.name !== 'HomePage') {
        if (!this.rest.loginAccessToken) {
          this.nav.setRoot('LoginPage')
        }
        else {
          this.nav.setRoot('HomePage');
        }


      }
    });
  }


  checkConnection() {
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.rest.connection = false;
      const alert = this.alertCtrl.create({
        title: 'Warning',
        message: 'Your are currently offline Please turn on your internet connection!',
        mode: 'ios',
        buttons: ['OK']
      });
      alert.present();
    });



    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      this.rest.connection = true;
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });
  }
recentRides()
{
  this.storage.get("recentrides").then(rides => {
    console.log("The user rides: ", rides);
    this.rest.recentRides = rides;
    
  }, error => {
    console.log("failed to retrieve user details")
   
  })
}

}
