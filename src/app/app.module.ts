import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import * as firebase from 'firebase';
import { RestProvider } from '../providers/rest/rest';

import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Network } from '@ionic-native/network';
import { InAppBrowser } from '@ionic-native/in-app-browser';
const config = {
  

  apiKey: "AIzaSyCSrnuA-oPA_iksDYhKOzaPIsQarkRIL14",
  authDomain: "rahisi-cabs.firebaseapp.com",
  databaseURL: "https://rahisi-cabs.firebaseio.com",
  projectId: "rahisi-cabs",
  storageBucket: "",
  messagingSenderId: "1011707328738",
  appId: "1:1011707328738:web:f347fbc1acfe9d7ec646bb"
  };
// production credentials
  // apiKey: "AIzaSyDsArfZkQQx7a1qTSxuRwvVCIxO6DQtvQM",
  // authDomain: "xena-cabs.firebaseapp.com",
  // databaseURL: "https://xena-cabs.firebaseio.com",
  // projectId: "xena-cabs",
  // storageBucket: "",
  // messagingSenderId: "1016098212472",
  // appId: "1:1016098212472:web:97ab178aa70c9167"

firebase.initializeApp(config);

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollAssist: true,
      autoFocusAssist: true
    }),
    HttpClientModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Geolocation, RestProvider, CallNumber, SocialSharing, LocationAccuracy,
    InAppBrowser,
    AndroidPermissions,
    Network
  ]
})
export class AppModule { }
