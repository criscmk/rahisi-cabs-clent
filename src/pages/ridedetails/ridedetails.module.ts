import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RidedetailsPage } from './ridedetails';

@NgModule({
  declarations: [
    RidedetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(RidedetailsPage),
  ],
})
export class RidedetailsPageModule {}
