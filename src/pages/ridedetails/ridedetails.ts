import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RidedetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ridedetails',
  templateUrl: 'ridedetails.html',
})
export class RidedetailsPage {

  ride: any;
  pickup_location: any;
  destination: any;
  cabmodel: any;
  cabplate: any;
  time: Date;
  distance: any;
  cost: any;
  date: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.ride = this.navParams.get('Ride')

    this.pickup_location = this.ride.start_location;
    this.destination = this.ride.destination_location;
    this.cabmodel = this.ride.ride_type;
    this.cabplate = this.ride.bike;
    this.distance = this.ride.distance.toFixed(0);
    this.cost = this.ride.cost;
    this.date = this.ride.created_at;
    console.log(this.ride)
  }

}
