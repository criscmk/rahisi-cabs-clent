import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController, ToastController, Platform, ModalController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import firebase from 'firebase';
import { RestProvider } from '../../providers/rest/rest';

declare var google;

/**
 * Generated class for the RideInvoiceModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride-invoice-modal',
  templateUrl: 'ride-invoice-modal.html',
})
export class RideInvoiceModalPage {
  directionsService: any
  directionsDisplay: any
  geocoder: any

  startName: string = ''
  endName: string = ''

  startPoint: any = {}
  destination: any = {}
  distance: number = 0

  startTime: any
  endTime: any
  travelTime: any
  totalCost: number = 0

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,
    private geolocation: Geolocation, private loadingCtrl: LoadingController, private alertCtrl: AlertController,
    private toastCtrl: ToastController, private platform: Platform,
    private restProvider: RestProvider, private modalCtrl: ModalController) {

    // retrieve ride details from firebase
    // firebase.database().ref('underwayRides/' + restProvider.user_details.phone).once('value', snapShot => {
    //   this.startPoint = snapShot.child('startLocation').val()
    //   this.destination = snapShot.child('destination').val()
    //   this.distance = snapShot.child('distance').val()
    //   this.travelTime = snapShot.child('travelTime').val()
    //   this.totalCost = snapShot.child('totalCost').val()
    // });

    // retrieve the starting point
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/startLocation').on('value', snapShot => {
      this.startPoint = snapShot.val()
    });

    // retrieve the destination
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/destination').on('value', snapShot => {
      this.destination = snapShot.val()
    });

    // retrieve the distance
    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone + '/distance').on('value', snapShot => {
      this.distance = snapShot.val()
    });

    // retrieve the travel time
    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone + '/travelTime').on('value', snapShot => {
      this.travelTime = snapShot.val()
      console.log('start time: ', this.startTime)
    });

    // retrieve the totaCost
    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone + '/totalCost').on('value', snapShot => {
      this.totalCost = snapShot.val()
    });



    this.geocoder = new google.maps.Geocoder;

    console.log('startPoint', this.startPoint);
    console.log('destination', this.destination);
    console.log('distance', this.distance);
    console.log('travelTime', this.travelTime);
    console.log('totalCost', this.totalCost);
    // console.log('destination', this.destination);
  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      content: 'Just a moment...',
      spinner: 'crescent'
    })

    loading.present()

    console.log('ionViewDidLoad RideInvoiceModalPage');
    this.geocodeStarting(this.geocoder)
    this.geocodeDestination(this.geocoder)

    loading.dismiss()

  }

  // get the name of the place, location
  geocodeStarting(geocoder) {
    geocoder.geocode({ 'location': this.startPoint }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.startName = results[0].formatted_address
          console.log("start name: ", this.startName)
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }

  // get the name of the place, location
  geocodeDestination(geocoder) {
    geocoder.geocode({ 'location': this.destination }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.endName = results[0].formatted_address
          console.log(" end name: ", this.endName)
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }


  //present modal
  presentModal() {

    // remove the ride 
    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone).remove().then(() => {
      console.log('ride removed')

      this.navCtrl.setRoot('RateModalPage')

    }, err => {
      this.alertCtrl.create({
        title: "Error",
        message: 'This operation could not be completed. Please try again.',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }, {
          text: 'Retry',
          handler: () => {
            this.presentModal()
          }
        }]
      })
    })

  }

  closeModal() {
    this.navCtrl.setRoot('HomePage')
  }

}
