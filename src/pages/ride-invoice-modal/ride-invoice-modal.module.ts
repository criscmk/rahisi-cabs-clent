import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideInvoiceModalPage } from './ride-invoice-modal';

@NgModule({
  declarations: [
    RideInvoiceModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RideInvoiceModalPage),
  ],
})
export class RideInvoiceModalPageModule {}
