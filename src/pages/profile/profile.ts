import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  name: any;
  email: any;
  phone: any;

  constructor(
    public navCtrl: NavController,
    public strg: Storage,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController, ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.strg.get('USER_PROFILE').then((val) => {
      console.log('Your profile is', val);
      this.name = val.name;
      this.email = val.email;
      this.phone = val.phone_number;
    });

  }

  goBack() {
    this.navCtrl.setRoot('HomePage');
  }

  change() {
    this.navCtrl.push('ChangePage')
  }

  accounts() {

  }

  onModelChange(rate) {
    console.log("star event", rate)
  }

}
