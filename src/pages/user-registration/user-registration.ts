import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the UserRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-registration',
  templateUrl: 'user-registration.html',
})
export class UserRegistrationPage {
  userData =
    {

    }

  inputType: string = 'password'
  registrationForm: FormGroup
  response: any;
  errorResponse: any = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private restProvider: RestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage, ) {
    this.registrationForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      phone: ['', Validators.compose([Validators.required, Validators.minLength(9), Validators.maxLength(13)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      passwordConfirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRegistrationPage');
  }

  // switch the input type of password field to reveal or hide password accordingly
  switchInputType(type) {
    if (type === 'password') {
      this.inputType = 'password'
    } else if (type === 'text') {
      this.inputType = 'text'
    }
  }

  // send user data to the server to register the new user

  signUp() {
    this.userData = {
      'name': this.registrationForm.controls.name.value,
      'email': this.registrationForm.controls.email.value,
      'phone': this.registrationForm.controls.phone.value,
      'password': this.registrationForm.controls.password.value,
    }

    console.log('registration data', this.userData)
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    loading.present();
    this.restProvider.templateLoader('/register', this.userData).then((result) => {
      loading.dismiss()
      console.log(result)
      this.response = result
      this.restProvider.loginAccessToken = this.response.access_token
      if (this.restProvider.loginAccessToken) {
        this.storage.set('STORAGE_KEY_TOKEN', this.response.access_token);
        this.getProfile();
        this.navCtrl.setRoot('HomePage');
        console.log('registration succesfull')
      }
      else {
        if (this.response.email) {
          this.errorResponse.push(this.response.email[0]);
        } if (this.response.phone) {
          this.errorResponse.push(this.response.phone[0])
        } else {
          return null
        }
        let arrayLength = this.errorResponse.length;
        let errMessage = ''
        this.errorResponse.forEach(element => {
          errMessage = errMessage + "\n" + element
        });
        console.log("arrayLength", arrayLength)
        this.alertCtrl.create({
          title: 'Sign Up Error',
          message: errMessage,
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {
              this.errorResponse = [];
            }
          }]
        }).present()
      }

    }, error => {
      loading.dismiss()
      this.alertCtrl.create({
        title: 'Server Error',
        message: 'This operation could not be completed. Please try again later.',
        mode: 'ios',
        buttons: [{
          text: 'Cancel',
          handler: () => {

          }
        }, {
          text: 'Retry',
          handler: () => {
            this.signUp()
          }
        }]
      })
    })
  }

  // navigate the user to sign in page
  signIn() {
    this.navCtrl.setRoot('LoginPage')
  }

  
  getProfile() {
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'crescent'
    })

    loading.present()

    this.restProvider.mainGet('/get-profile').
      subscribe(res => {
        loading.dismiss();

        this.storage.set('profile', res)
        let details: any = []
        details = res
        this.restProvider.user_details = details;
        this.restProvider.name = details.name;
        this.restProvider.email = details.email;
        this.restProvider.phone = details.phone;

      }, error => {
        loading.dismiss();
        console.log("Error", error)
        this.alertCtrl.create({
          title: 'Error',
          message: 'Failed to retrieve your details. Please try again.',
          mode: 'ios',
          buttons: [{
            text: 'Cancel',
            handler: () => {
              this.navCtrl.setRoot('HomePage')
            }
          }, {
            text: 'Retry',
            handler: () => {
              this.getProfile()
            }
          }]
        }).present()
      });

  }
}
