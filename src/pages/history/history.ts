import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';



/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  rides: any;
  isReady: boolean = false
  ready: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private rest: RestProvider,
    private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
    this.getRidesHistory()
  }

  //get driver rides history

  getRidesHistory() {

    // var user = this.rest.user_id;
    // let userID = {
    //   "user_id": this.rest.user_details.id
    // }

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })

    loading.present()
    this.rest.mainGet('/user-ride-history').subscribe((res) => {
      this.rides = res
      loading.dismiss()
      this.ready = true;
      console.log('History',this.rides);

    
      if(this.rides.status==201)
      {
        this.isReady = false
      }
      else{
        this.isReady = true
        this.rides = this.rides;
      }

      console.log("my rides", this.rides);
    }, error => {
      loading.dismiss();
      console.log("error", error)
      this.alertCtrl.create({
        title: 'Error',
        message: 'The operation could not be completed. Please try again.',
        mode: 'ios',
        buttons: [{
          text: 'Cancel',
          handler: () => {
            this.navCtrl.setRoot('HomePage')
          }
        }, {
          text: 'Retry',
          handler: () => {
            this.getRidesHistory()
          }
        }]
      }).present()

    });
  }

  public viewRide(ride) {
    this.navCtrl.push('RidedetailsPage', { 'Ride': ride })
  }

}
