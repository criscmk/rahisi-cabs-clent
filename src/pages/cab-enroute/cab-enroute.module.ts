import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CabEnroutePage } from './cab-enroute';

@NgModule({
  declarations: [
    CabEnroutePage,
  ],
  imports: [
    IonicPageModule.forChild(CabEnroutePage),
  ],
})
export class CabEnroutePageModule {}
