import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import * as firebase from 'firebase';
import { RestProvider } from '../../providers/rest/rest';
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';

declare var google;

/**
 * Generated class for the CabEnroutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cab-enroute',
  templateUrl: 'cab-enroute.html',
})
export class CabEnroutePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  directionsService: any;
  directionsDisplay: any;

  currentLocation: any = { lat: 0.0236, lng: 37.9062 };
  userMarker: any;
  cabLocation: any = { lat: 0.0236, lng: 37.9062 };
  cabMarker: any = {};

  driverName: string = '';
  driverPhoneNumber: string = '';
  vehicleRegistration: string = '';
  driverProfile: string = '';

  arrivalTime: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private zone: NgZone,
    private geolocation: Geolocation, private platform: Platform, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private restProvider: RestProvider, private callNumber: CallNumber) {

    // watch the ride status then navigate the user accordingly
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').on('value', snapShot => {

      let rideStatus = snapShot.val()

      console.log("journey status: ", rideStatus)

      if (rideStatus === 'started') {
        firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').off()
        // journey has been started, navigate the user to underway journey page
        navCtrl.setRoot('JourneyUnderwayPage')
      } else if (rideStatus === 'cancelled') {
        firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').off()
        alertCtrl.create({
          title: 'Ride Cancelled',
          message: 'The driver cancelled your ride. Please make a new request.',
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {
              firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').off()
              navCtrl.setRoot('HomePage')
            }
          }]
        }).present()
      } else {
        return
      }
    });

    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone).once('value', snapShot => {

      console.log('data: ', snapShot.val())

      restProvider.driverID = snapShot.child('driverID').val();
      this.driverName = snapShot.child('driverName').val();
      this.restProvider.driverName = this.driverName;
      this.driverPhoneNumber = snapShot.child('driverPhoneNumber').val();
      this.restProvider.driverPhoneNumber = this.driverPhoneNumber;
      this.vehicleRegistration = snapShot.child('vehicleRegistration').val();
      this.restProvider.vehicleRegistration = this.vehicleRegistration;
      this.driverProfile = snapShot.child('Driver_Profile').val();
      this.arrivalTime = snapShot.child('arrivalTime').val();
      this.cabLocation = snapShot.child('cabLocation').val();

    }, err => {
      console.error('An error occured retrieving firebase data');
      alertCtrl.create({
        message: 'Some info did not load correctly.',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      })
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CabEnroutePage');
    this.getUserLocation()
  }

  callDriver() {
    this.callNumber.callNumber(this.driverPhoneNumber, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }

  getUserLocation() {
    let loading = this.loadingCtrl.create({
      content: 'Just a moment...',
      spinner: 'crescent'
    })

    loading.present()
    this.platform.ready().then(() => {

      this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
        this.currentLocation = { lat: resp.coords.latitude, lng: resp.coords.longitude }

        this.initializeMap()

        this.map.setCenter(this.currentLocation)
        this.userMarker.setPosition(this.currentLocation)

        loading.dismiss()


        // track the cab location and update it on the map
        firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone + '/cabLocation').once('value', snapShot => {

          console.log('Phone: ', this.restProvider.user_details.phone)
          this.cabLocation = snapShot.val()

          console.log('new cab location is: ', this.cabLocation)

          this.cabMarker.setPosition(this.cabLocation)
          // this.displayRoute(this.cabLocation, this.currentLocation, this.directionsService,
          //   this.directionsDisplay);

        })

      }).catch((error) => {
        loading.dismiss()
        console.log('Error getting location', error);
      });

    });
  }

  initializeMap() {

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: this.currentLocation,
      disableDefaultUI: true,
      zoom: 14,
    });

    let infowindow = new google.maps.InfoWindow({
      content: "My Location"
    });

    this.userMarker = new google.maps.Marker({
      position: this.currentLocation,
      map: this.map,
      animation: google.maps.Animation.DROP,
    });

    // custom marker for the cab
    let cabIcon = {
      url: 'assets/imgs/car.png',
      scaledSize: new google.maps.Size(35, 35), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    }

    // set the marker of the cab on the map
    this.cabMarker = new google.maps.Marker({
      position: this.currentLocation,
      map: this.map,
      icon: cabIcon,
    });

    // add a click listener to show infoWindow
    this.userMarker.addListener('click', function () {
      infowindow.open(this.map, this.userMarker);
    });

    // initialize the maps services
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      map: this.map,
    });
  }

  // cancel the request
  cancelRequest() {
    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone + '/rideStatus').off()

    let loading = this.loadingCtrl.create({
      content: 'Cancelling Ride...',
      spinner: 'crescent'
    })

    loading.present()

    // update the status in underwayrides 
    firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone)
      .update({ rideStatus: 'cancelled' }).then(() => {
        console.log('underway Ride cancelled')

        // remove the ride 
        firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone).remove().then(() => {
          console.log('ride cancelled')

          loading.dismiss()

        })
      })
    console.log("Cacelled..")
    this.navCtrl.setRoot('HomePage')
  }

  // display the route 
  displayRoute(origin, destination, service, display) {
    if (origin != destination) {
      service.route({
        origin: origin,
        destination: destination,
        travelMode: 'DRIVING',
        avoidTolls: true
      }, (response, status) => {
        if (status === 'OK') {
          display.setDirections(response);
        } else {
          console.log('Could not display directions due to: ' + status);
        }
      });
    }
  }

}
