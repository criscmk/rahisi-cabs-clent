import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import swal from 'sweetalert';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  inputType: string = 'password'
  loginForm: FormGroup
  response: any;

  constructor(
    public navParams: NavParams, private formBuilder: FormBuilder, private toastCtrl: ToastController,
    private restProvider: RestProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController,
    private navCtrl: NavController, private storage: Storage) {

    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  // switch the input type of password field to reveal or hide password accordingly
  switchInputType(type) {
    if (type === 'password') {
      this.inputType = 'password'
    } else if (type === 'text') {
      this.inputType = 'text'
    }
  }

  // send user credentials to the server for validation
  signIn() {
    console.log('login details', this.loginForm.value);

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'crescent'
    })

    loading.present()

    this.restProvider.templateLoader('/login', this.loginForm.value).then((result) => {
      // loading.dismiss()
      console.log(result)

      this.response = result

      this.restProvider.loginAccessToken = this.response.access_token

      if (this.restProvider.loginAccessToken) {
        loading.dismiss();
        this.storage.set('STORAGE_KEY_TOKEN', this.response.access_token);
        this.navCtrl.setRoot('HomePage')
        this.getProfile()
      }
      else {
        loading.dismiss();
        this.sweet();
         
              
      }
    }, error => {
      loading.dismiss()
      this.sweet();
      // console.log("error: ", error)
      // this.alertCtrl.create({
      //   title: 'Sign in Error',
      //   message: 'Invalid username/ password',
      //   mode: 'ios',
      //   buttons: [{
      //     text: 'Dismiss',
      //     handler: () => {

      //     }
      //   }]
      // }).present()
    });

  }

  // navigate the user to the registration page
  newUser() {
    this.navCtrl.push('UserRegistrationPage')
  }

  // get user details
  getProfile() {
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'crescent'
    })

    loading.present()

    this.restProvider.mainGet('/get-profile').
      subscribe(res => {
        loading.dismiss();
        this.storage.set('profile', res)
        let details: any = [];
        details = res;
        this.restProvider.user_details = details;
        this.restProvider.name = details.name;
        this.restProvider.email = details.email;
        this.restProvider.phone = details.phone;

      }, error => {
        loading.dismiss();
        console.log("Error", error)
        this.alertCtrl.create({
          title: 'Error',
          message: 'Failed to retrieve your details. Please try again.',
          mode: 'ios',
          buttons: [{
            text: 'Cancel',
            handler: () => {
              this.navCtrl.setRoot('HomePage')
            }
          }, {
            text: 'Retry',
            handler: () => {
              this.getProfile()
            }
          }]
        }).present()
      });

  }
  sweet()
  {
    swal({
      title: "Warning!",
      text: "Incorrect username/password",
      icon: "error",
      
    });
  }
}
