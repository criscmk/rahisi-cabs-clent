import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController, ViewController, ToastController } from 'ionic-angular';
import * as firebase from 'firebase';
import { RestProvider } from '../../providers/rest/rest';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage'
import { CallNumber } from '@ionic-native/call-number';
declare var google;

// convert datasnapshot to an array
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  salam: any;
  maptype: number = 1;
  recent: any;
  recentsafari: {};
  recentplace: {};
  directionset: boolean;
  recentb: any;
  showrecentrides: any = [];
  [x: string]: any;
  @ViewChild('map') mapElement: ElementRef;
  map: any;

  directionsService: any;
  directionsDisplay: any;
  matrixService: any;

  googleAutocomplete: any;
  autocomplete: any = { startingPoint: '', destination: '' };
  startingPointItems: any = [];
  destinationItems: any = [];

  currentLocation: any = { lat: -1.265320435161434, lng: 36.8039631843567 };
  destination: any = {};
  geocoder: any;

  markers: any = [];
  userMarker: any;


  durationText: string = '0 Mins';
  distanceText: string = '0 KM';

  computedDistance: number = 0;

  isCalculating: boolean = false;

  isRequesting: boolean = false;
  isJourneyStarted: boolean = false;
  isShown: boolean = false;
  isDestinationSet: boolean = false;

  rideStatus: string = '';
  cabs: any = [];
  cabMarkers: any = [];
  nearCabs: any = []

  watch: any;
  fare: any;
  faresRates: any;

  rideType: any = 0;
  totalFareEco: number = 150;
  totalFareBis: number = 200;
  totalFare: number = 50;
  rideTypeName: string;
  totalBodaFare: number;
  geo: any

  service = new google.maps.places.AutocompleteService();
  autocompleteItems: any[];
  latitude: any;
  longitude: any;
  state: number;
  myDate: any;
  hrs: any;
  myrecentRides: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toastCtrl: ToastController,
    public viewCtrl: ViewController, private zone: NgZone,
    private iab: InAppBrowser,
    private callNumber: CallNumber,
    public storage: Storage,
    private geolocation: Geolocation, private platform: Platform, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private restProvider: RestProvider, private http: HttpClient) {
    this.googleAutocomplete = new google.maps.places.AutocompleteService();
    this.matrixService = new google.maps.DistanceMatrixService();
    this.geocoder = new google.maps.Geocoder;
    this.trackRequest();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    this.myDate = new Date();
    this.hrs = this.myDate.getHours();
    var greet;
    if (this.hrs < 12)
      greet = 'Good Morning';
    else if (this.hrs >= 12 && this.hrs <= 17)
      greet = 'Good Afternoon';
    else if (this.hrs >= 17 && this.hrs <= 24)
      greet = 'Good Evening';
    console.log("greetings", greet);
    this.salam = greet
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.maptype = this.restProvider.maptype;
    this.recent = this.restProvider.recentRides;
    console.log('my recent', this.recent);
    console.log('map type', this.maptype);
    this.getUserLocation();
    this.getFareCharges();
    console.log('user name', this.restProvider.user_details.name);
    this.state = 0;
    let a = this.recent;
    if (this.recent != null) {
      this.ShuffleRecentplaces(a);
    }


  }
  //get fare charges 
  getFareCharges() {
    this.restProvider.mainGet('/get-fare-rates').
      subscribe((res) => {
        this.faresRates = res
        this.fare = this.faresRates.fare_rates;
        console.log('set fares', this.fare);

        this.restProvider.minFare = this.fare[0].minimum_fare;
        this.restProvider.costperKM = this.fare[0].cost_per_km;
        this.restProvider.costperMinute = this.fare[0].cost_per_minute;
        this.restProvider.baseFare = this.fare[0].base_fare;
        this.restProvider.baseFareEco = this.fare[1].base_fare;
        this.restProvider.minFareEco = this.fare[1].minimum_fare;
        this.restProvider.costperKMEco = this.fare[1].cost_per_km;
        this.restProvider.costperMinuteEco = this.fare[1].cost_per_minute;
        this.restProvider.radius = this.faresRates.radius_duaration.radius;
        this.restProvider.duration = this.faresRates.radius_duaration.duration

        console.log("fare charges", res);
        console.log("minimum farw", this.restProvider.minFareEco);
        console.log("base fare", this.restProvider.baseFareEco);
        console.log("cost per min", this.restProvider.costperMinuteEco);
        console.log("cost per km", this.restProvider.costperKMEco);
      }, error => {
        console.log("error", error);
      })
  }

  // get the current user location
  getUserLocation() {

    let loading = this.loadingCtrl.create({
      content: 'Just a moment...',
      spinner: 'crescent'
    })

    loading.present()

    this.platform.ready().then(() => {

      let mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
      }

      this.userMarker = new google.maps.Marker({
        position: this.currentLocation,
        map: this.map,
        animation: google.maps.Animation.DROP,
        draggable: true
      });

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {

        loading.dismiss();

        this.currentLocation = { lat: resp.coords.latitude, lng: resp.coords.longitude }
        console.log("My location: ", this.currentLocation)
        this.geocodeLatLng(this.geocoder)

        // this.initializeMap()
        // this.watchLocation()

        this.map.setCenter(this.currentLocation);
        this.userMarker.setPosition(this.currentLocation);
        this.map.setZoom(15);
        let infowindow = new google.maps.InfoWindow({
          content: "My Location"
        });

        this.userMarker.addListener('click', () => {
          infowindow.open(this.map, this.userMarker);
        });


        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer({
          map: this.map,
        });

        google.maps.event.addListener(this.userMarker, 'drag', (evt) => {
          this.map.setCenter(this.userMarker.getPosition());
          this.currentLocation = this.userMarker.getPosition();
          this.geocodeLatLng(this.geocoder)

          if (this.isDestinationSet) {
            this.displayRoute(this.currentLocation, this.destination, this.directionsService,
              this.directionsDisplay);
          }

          console.log("new position: ", this.userMarker.getPosition())
          console.log('new address: ', this.autocomplete.starting)
        });

        this.directionsDisplay.addListener('directions_changed', () => {
          this.fecthJourneyData()
        });

        this.addCabs()

      }).catch((error) => {
        loading.dismiss()
        console.log('Error getting location', error);
      });

    });
  }

  // get the name of the place, current location of the user
  geocodeLatLng(geocoder) {

    geocoder.geocode({ 'location': this.currentLocation }, (results, status) => {
      if (status === 'OK') {
        console.log("results", results)
        if (results[0]) {
          this.autocomplete.startingPoint = results[0].formatted_address
          console.log("location name: ", results[0].formatted_address)
        } else {
          console.log('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }

  watchLocation() {
    let options = {
      frequency: 30000,
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options).subscribe(position => {
      if ((position as Geoposition).coords != undefined) {
        var geoposition = (position as Geoposition);

        // Run update inside of Angular's zone
        this.zone.run(() => {
          this.currentLocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }

          this.userMarker.setPosition(this.currentLocation)
          this.map.setCenter(this.currentLocation)
          this.geocodeLatLng(this.geocoder)

        });

      } else {

        console.log('Error retrieving location')
      }
    });
  }

  initializeMap() {

    // center: this.currentLocation,
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      disableDefaultUI: true,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    })

    let infowindow = new google.maps.InfoWindow({
      content: "My Location"
    });

    this.userMarker = new google.maps.Marker({
      position: this.currentLocation,
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable: true
    });

    this.userMarker.addListener('click', () => {
      infowindow.open(this.map, this.userMarker);
    });


    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      map: this.map,
    });

    google.maps.event.addListener(this.userMarker, 'drag', (evt) => {
      this.map.setCenter(this.userMarker.getPosition());
      this.currentLocation = this.userMarker.getPosition();
      this.geocodeLatLng(this.geocoder)

      if (this.isDestinationSet) {
        this.displayRoute(this.currentLocation, this.destination, this.directionsService,
          this.directionsDisplay);
      }

      console.log("new position: ", this.userMarker.getPosition())
      console.log('new address: ', this.autocomplete.starting)
    });

    this.directionsDisplay.addListener('directions_changed', () => {
      this.fecthJourneyData()
    });

    this.addCabs()
  }

  // search for a starting point using autocomplete feature
  searchStartingPoint() {

    if (this.autocomplete.startingPoint == '') {
      this.startingPointItems = [];
      return;
    }

    var kenyaBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(0.0236, 37.9062));

    this.googleAutocomplete.getPlacePredictions({
      input: this.autocomplete.startingPoint, bounds: kenyaBounds,
      strictBounds: true
    },
      (predictions, status) => {
        this.startingPointItems = [];
        this.zone.run(() => {
          if (predictions) {
            predictions.forEach((prediction) => {
              this.startingPointItems.push(prediction);
            });
          } else {
            console.log("no predictions")
          }
        });
      });
  }

  // select the starting point from the list of suggestions
  selectStartingPoint(item) {
    this.clearMarkers();
    this.startingPointItems = [];

    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      console.log(results)
      if (status === 'OK' && results[0]) {
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };

        this.currentLocation = null
        this.currentLocation = position

        this.userMarker.setPosition(this.currentLocation);

        this.map.setCenter(this.currentLocation);

        this.autocomplete.startingPoint = results[0].formatted_address
      }
    })
  }

  // search for the destination using autcomplete feature 
  searchDestination() {

    if (this.autocomplete.destination == '') {
      this.destinationItems = [];
      return;
    }

    var kenyaBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(0.0236, 37.9062));

    this.googleAutocomplete.getPlacePredictions({
      input: this.autocomplete.destination, bounds: kenyaBounds,
      strictBounds: true
    },
      (predictions, status) => {
        this.destinationItems = [];
        this.zone.run(() => {
          if (predictions) {
            predictions.forEach((prediction) => {
              this.destinationItems.push(prediction);
            });

          } else {
            console.log("no predictions")
          }
        });
      });
  }

  //select the destination from the list of suggestions
  selectDestination(item) {
    this.clearMarkers();
    this.destinationItems = [];


    console.log("place id", item)
    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      console.log("results", results)
      if (status === 'OK' && results[0]) {
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };

        this.destination = position;


        console.log("destination: ", this.destination);
        console.log("selected place", this.autocomplete.destination);


        this.displayRoute(this.currentLocation, this.destination, this.directionsService,
          this.directionsDisplay);

        this.fecthJourneyData();

        // this.autocomplete.destination = results[0].formatted_address;
        this.autocomplete.destination = item.structured_formatting.main_text;
        this.isDestinationSet = true;
        // this.rideType = 1;
        // this.rideTypeName = 'boda';

      }
    })
  }

  // remove the markers 
  clearMarkers() {
    this.markers = [];
  }

  // display the route 
  displayRoute(origin, destination, service, display) {
    service.route({
      origin: origin,
      destination: destination,
      travelMode: 'DRIVING',
      avoidTolls: true
    }, (response, status) => {
      if (status === 'OK') {
        display.setDirections(response);
      } else {
        alert('Could not display directions due to: ' + status);
      }
    });

  }

  // get journey details from distance matrix api
  fecthJourneyData() {

    this.isCalculating = true

    let origin = new google.maps.LatLng(this.currentLocation);
    let destination = new google.maps.LatLng(this.destination);

    this.matrixService.getDistanceMatrix({
      origins: [origin],
      destinations: [destination],
      travelMode: 'DRIVING',
      drivingOptions: {
        departureTime: new Date(Date.now()),
        trafficModel: 'optimistic'
      },
      avoidHighways: true,
      avoidTolls: true,
    }, (response, status) => {

      this.isCalculating = false

      if (status == 'OK') {
        let origins = response.originAddresses;
        let destinations = response.destinationAddresses;

        for (let i = 0; i < origins.length; i++) {
          let results = response.rows[i].elements;

          for (let j = 0; j < results.length; j++) {
            let element = results[j];

            this.distanceText = element.distance.text;
            this.durationText = element.duration.text;

            let distanceValue = element.distance.value
            let durationValue = element.duration.value
            this.computedDistance = distanceValue / 1000

            let from = origins[i];
            let to = destinations[j];

            console.log('From: ', from, ', to: ', to)
            this.computeFare(distanceValue, durationValue)
          }
        }

      } else {
        console.log('Failed to get details from the matrix api')
      }

    });
  }

  // compute the fare of the journey
  computeFare(rawDistance, rawDuration) {

    let dist = rawDistance / 1000
    let duration = rawDuration / 60

    console.log('Calculated distance: ', dist, ', duration: ', duration);

    let totalBoda: any = Math.round((this.restProvider.baseFare + (dist * this.restProvider.costperKM) + (duration * this.restProvider.costperMinute)) / 10) * 10;
    let totalEco: any = Math.round((this.restProvider.baseFareEco + (dist * this.restProvider.costperKMEco) + (duration * this.restProvider.costperMinuteEco)) / 10) * 10;
    let totalBis: any = Math.round((this.restProvider.baseFareBis + (dist * this.restProvider.costperKMBis) + (duration * this.restProvider.costperMinuteBis)) / 10) * 10;
    let totalAmount = parseFloat(totalBoda)
    if (totalAmount < this.restProvider.minFare) {
      this.totalBodaFare = this.restProvider.minFare
    } else {
      this.totalBodaFare = totalAmount
    };

    let totalAmountEco = parseFloat(totalEco)
    if (totalAmountEco < this.restProvider.minFareEco) {
      this.totalFareEco = this.restProvider.minFareEco
    } else {
      this.totalFareEco = totalAmountEco
    }

    let totalAmountBis = parseFloat(totalBis)
    if (totalAmountBis < this.restProvider.minFareBis) {
      this.totalFareBis = this.restProvider.minFareBis
    } else {
      this.totalFareBis = totalAmountBis
    }

    console.log('Total Fare: ', this.totalFare)

  }

  // send request to nearby cabs
  requestRide() {
    if (this.rideType == 0) {
      this.alertCtrl.create({
        title: 'Attention!!!',
        message: 'Choose mode of ride',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      }).present()
    }
    else {
      this.isRequesting = true;
      let cabsAround: boolean = false;

      // watch the cabs node for any changes and update the cabs array accordingly
      firebase.database().ref('nearbycab/').once('value', resp => {
        this.cabs = [];
        this.nearCabs = [];
        this.cabs = snapshotToArray(resp);

        console.log('Cabs length: ', this.cabs.length);
        console.log('Cabs details: ', this.cabs)

        // compute the distance between the user location and nearby cabs

        for (let i = 0; i < this.cabs.length; i++) {

          let pointA = new google.maps.LatLng(this.currentLocation);
          let pointB = new google.maps.LatLng(this.cabs[i].cabLocation);

          let dist = (google.maps.geometry.spherical.computeDistanceBetween(pointA, pointB) / 1000).toFixed(2)

          console.log('distance: ', dist)

          if (parseInt(dist) <= this.restProvider.radius) {
            this.nearCabs.push(this.cabs[i].fcmToken)
            console.log('token', this.cabs[i].fcmToken)
          }

          console.log("fcm token", this.nearCabs);



          // check if the cabs are within radius
          console.log('estimated distance btwn driver and cleint', dist);
          if (parseInt(dist) <= this.restProvider.radius) {
            cabsAround = true
            break
          } else {
            cabsAround = false
          }

          console.log('cabsAround? ', cabsAround)
        }

        if (cabsAround) {

          console.log('There are cabs nearby, proceeding with placing the request')
          let currDate: any = new Date().toISOString();
          console.log("date ", currDate.toLocaleString('EST'))
          // let currDate = new moment().format()
          this.restProvider.startLocation = this.currentLocation
          this.restProvider.destination = this.destination
          this.restProvider.distance = this.computedDistance
          let requestData = {
            rideStatus: 'requesting',
            rideType: this.rideType,
            rideTypeName: this.rideTypeName,
            startLocation: this.currentLocation,
            destination: this.destination,
            name: this.restProvider.user_details.name,
            distance: this.computedDistance,
            cost: this.totalFare,
            phoneNumber: this.restProvider.user_details.phone,
            cabLocation: { lat: 0, lng: 0 },
            userID: this.restProvider.user_details.id,
            time: currDate,
            duration: this.durationText
          }
          console.log('key primary', this.restProvider.user_details.phone);

          // create a new request
          firebase.database().ref('newRequests/' + this.restProvider.user_details.phone).set(requestData).then(() => {
            console.log('Request sent')
            // add a new instance of the ride to underwayrides to keep track of the ride status
            firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone).set(requestData).then(() => {
              console.log('Underway ride initiated')
              this.trackRequest();
              console.log('near cabs length: ', this.nearCabs.length)

              this.nearCabs.forEach(token => {
                this.sendNotification(requestData, token)
              });

              // this.testnotification();
              // this.sendNotification(requestData);

            });
          });

        } else {
          this.alertCtrl.create({
            title: 'Out of Range',
            message: 'You are too far away from the range of our drivers. Call 0711984124',
            mode: 'ios',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  this.isRequesting = false
                }
              },
              {
                text: 'Call',
                handler: () => {
                  this.isRequesting = false
                  console.log('Buy clicked');
                  this.callNumber.callNumber("0711984124", true)
                    .then(res => console.log('Launched dialer!', res))
                    .catch(err => console.log('Error launching dialer', err));
                }
              }
            ]
          }).present()
        }

      })
    }

  }

  //track request status in firebase
  trackRequest() {
    //check whether user is requesting
    if (this.isRequesting) {
      // watch the status of a new request and when the request is accepted navigate the user to cab-enroute page
      firebase.database().ref('newRequests/' + this.restProvider.user_details.phone + '/rideStatus').on('value', snapShot => {

        this.rideStatus = snapShot.val()
        console.log('ride status: ', this.rideStatus)

        if (this.rideStatus === 'accepted') {
          firebase.database().ref('newRequests/' + this.restProvider.user_details.phone + '/rideStatus').off()
          // ride is accepted, navigate the user to cab enroute page

          this.presentToast();

        } else {
          return
        }

      });
    }
    else {
      return
    }
  }

  // cancel the request
  cancelRequest() {

    let loading = this.loadingCtrl.create({
      content: 'Cancelling Ride...',
      spinner: 'crescent'
    })

    loading.present()

    firebase.database().ref('newRequests/' + this.restProvider.user_details.phone)
      .update({ rideStatus: 'cancelled' }).then(() => {
        console.log('Requests Ride cancelled')
        // update the status in underwayrides as well and the cab location
        firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone)
          .update({ rideStatus: 'cancelled' }).then(() => {
            console.log('underway Ride cancelled')

            // remove the request to stop broadcasting
            firebase.database().ref('newRequests/' + this.restProvider.user_details.phone).remove().then(() => {
              console.log('Request removed')
              // remove the request to stop broadcasting
              firebase.database().ref('underwayRides/' + this.restProvider.user_details.phone).remove().then(() => {
                console.log('ride cancelled')

                loading.dismiss()
              })
            })
          })
      })

    console.log("Cacelled..")
    this.isRequesting = false
  }

  // add / update marker positions on map
  addCabs() {
    // custom marker for the cab
    let cabIcon = {
      url: 'assets/imgs/car.png',
      scaledSize: new google.maps.Size(35, 35), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    }

    // watch the cabs node for any changes and update the cabs array accordingly
    firebase.database().ref('nearbycab/').on('value', resp => {
      this.cabs = [];
      this.cabs = snapshotToArray(resp);

      console.log('Cabs length: ', this.cabs.length)

      for (let i = 0; i < this.cabs.length; i++) {

        // computeDistanceBetween()

        let pointA = new google.maps.LatLng(this.currentLocation);
        let pointB = new google.maps.LatLng(this.cabs[i].cabLocation);

        let dist = (google.maps.geometry.spherical.computeDistanceBetween(pointA, pointB) / 1000).toFixed(2)

        if (parseInt(dist) <= 5) {

          this.cabMarkers[this.cabs[i].key] = new google.maps.Marker({
            map: this.map,
            icon: cabIcon
          });

          let newPosition = resp.child(this.cabs[i].key).child('cabLocation').val()
          this.cabMarkers[this.cabs[i].key].setPosition(newPosition)

          google.maps.event.addListener(this.cabMarkers[this.cabs[i].key], 'click', ((marker, i) => {
            return () => {

              let infowindow = new google.maps.InfoWindow({
                content: this.cabs[i].vehicleRegistration
              });

              infowindow.open(this.map, marker);
            }
          })(this.cabMarkers[this.cabs[i].key], i));

        }

      }

      firebase.database().ref('nearbyCabs/').off()
    });

  }

  toggleRide(value) {
    if (this.autocomplete.query == '') {
      this.alertCtrl.create({
        title: 'Attention!!!',
        message: 'Enter your destination',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      }).present()
    }
    else {

      this.rideType = value;
      console.log(this.rideType);
      if (this.rideType == 1) {
        this.rideTypeName = 'boda';
        this.totalFare = this.totalBodaFare;
        console.log('boda fare', this.totalBodaFare);

      } else if (this.rideType == 2) {
        this.rideTypeName = 'cab';
        this.totalFare = this.totalFareEco;
        console.log('Taxifare', this.totalFareEco);
      }
    }

  }

  sendNotification(requestData, driverToken) {

    let data = {
      "notification": {
        "title": "New Ride!",
        "body": "A customer has just ordered a ride. Click to view.",
        "sound": "/res/raw/bell.mp3",
        "click_action": "FCM_PLUGIN_ACTIVITY",
        "icon": "fcm_push_icon"
      },
      "data": requestData,
      'to': driverToken,
      "priority": "high",
      "restricted_package_name": ""
    }

    this.restProvider.postNotification('https://fcm.googleapis.com/fcm/send', JSON.stringify(data)).then(result => {

      console.log('Success! ', result);

      setTimeout(() => {
        if (this.isRequesting) {
          this.cancelRequest();
          this.alertCtrl.create({
            title: 'Unavailable',
            message: 'All our drivers seem busy at the moment. Call 0711984124',
            mode: 'ios',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'Call',
                handler: () => {
                  console.log('Buy clicked');
                  this.isRequesting = false
                  this.callNumber.callNumber("0711984124", true)
                    .then(res => console.log('Launched dialer!', res))
                    .catch(err => console.log('Error launching dialer', err));
                }
              }
            ]
          }).present()
        }
      }, this.restProvider.duration);
    }, (err) => {
      console.log('Failed! ', err)
    });

  }

  //sending notifications
  testnotification() {
    let body = {
      "notification": {
        "title": "New Ride!",
        "body": "A customer has just ordered a ride. Click to view.",
        "sound": "/res/raw/bell.mp3",
        "click_action": "FCM_PLUGIN_ACTIVITY",
        "icon": "fcm_push_icon"
      },
      "data": {
        "param1": "value1",
        "param2": "value2"
      },
      "to": "/topics/all",
      "priority": "high",
      "restricted_package_name": ""
    }
    let options = new HttpHeaders().set('Content-Type', 'application/json');
    this.http.post("https://fcm.googleapis.com/fcm/send", body, {
      headers: options.set('Authorization', 'key=AAAA6450uOI:APA91bFMHvRyJ38LxKOhWPhjrztxw-SU6dd6tBE7Yhma3ZUfSBMxnS0hFCqvHhZT9cEYc6Bc-4A9L2o2YE69xP9acdoIcOpi1svG10EPThGb7n3rzvm9YqjsjxXSW8AN5gZ35mkZJFIM'),
    }).subscribe(data => {
      console.log('Notification sent Successfully! method2 ', data)

    }, error => {
      console.log(error)
    });

  }

  // hide the bottom div on input focus
  checkFocus() {
    this.isShown = true;
  }

  // display the bottom div when the input field is out of focus
  checkBlur() {
    this.isShown = false;
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    //this.viewCtrl.dismiss(item);
    this.autocompleteItems = [];
    this.geo = item;
    this.geoCode(this.geo);//convert Address to lat and long
    console.log(this.autocomplete.query);
  }

  updateSearch() {

    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions({
      input: this.autocomplete.query,
      componentRestrictions: {
        country: 'ke'
      }
    }, (predictions, status) => {
      me.autocompleteItems = [];

      me.zone.run(() => {
        if (predictions != null) {
          predictions.forEach((prediction) => {
            me.autocompleteItems.push(prediction.description);
          });
        }
      });
    });
  }

  //convert Address string to lat and long
  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': address }, (results, status) => {
      this.autocomplete.query = address;
      this.latitude = results[0].geometry.location.lat();
      this.longitude = results[0].geometry.location.lng();
      if (status === 'OK' && results[0]) {
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        this.destination = position;
        console.log('destination', this.destination);
        this.displayRoute(this.currentLocation, this.destination, this.directionsService,
          this.directionsDisplay);
        this.autocomplete.destination = address;
        this.isDestinationSet = true;
        this.recentsafari = {
          destination: address,
          cord: this.destination
        }
        console.log("recent safari", this.recentsafari);

        this.showrecentrides.push(this.recentsafari)

        this.recent = this.showrecentrides;
        this.restProvider.recentRides = this.recent
        this.restProvider.maptype = 2;
        this.directionset = true
        console.log("recent rides", this.recent);
        this.storage.set("recentrides", this.restProvider.recentRides);

      }


    });
  }
  back1() {
    this.state = 0;
  }

  back2() {
    this.state = 0;
    this.directionsDisplay.set('directions', null);
  }
  setdestination() {

    this.state = 1;
    this.autocomplete.query = "";
  }
  cleardestination() {


    if (this.directionset) {
      this.directionsDisplay.set('directions', null);
    }
    this.directionset = false;

    this.autocomplete.query = "";
    this.destination = "";
    this.isDestinationSet = false;
    this.rideType = 0;



  }
  clearLocation1() {

  }

  //get app version
  getAppVersion() {
    this.restProvider.mainGet('/get-client-version').subscribe((result) => {
      console.log('App version: ', result)
      let res: any = []
      res = result

      if (parseInt(res.status) === 0) {
        this.alertCtrl.create({
          title: "Error",
          message: "Failed to retrieve app version. Please try again.",
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {

            }
          }, {
            text: 'Retry',
            handler: () => {
              this.getAppVersion()
            }
          }]
        }).present()
      } else if (parseInt(res.status) === 200) {

        if (res.version_number != this.restProvider.currentAppVersion) {
          this.alertCtrl.create({
            title: 'Update Found',
            message: 'There is a new update of the app. You are using version <strong>'
              + this.restProvider.currentAppVersion +
              '</strong> which will be made obsolete on <strong>' + res.date_of_update + '</strong>. Update now to version <strong>'
              + res.version_number + '<strong>.',
            mode: 'ios',
            buttons: [{
              text: 'Later',
              handler: () => {

              }
            }, {
              text: 'Update Now',
              handler: () => {
                const options: InAppBrowserOptions = {
                  zoom: 'no'
                }

                // Opening a URL and returning an InAppBrowserObject
                const browser = this.iab.create(this.restProvider.playstoreUrl, '_system', options);
              }
            }]
          }).present()
        }

      } else {
        this.toastCtrl.create({
          message: 'Could not fetch app version.',
          position: 'bottom',
          duration: 3000
        }).present()
      }
    }, err => {
      console.log('Error while fetching app version')
      this.toastCtrl.create({
        message: 'Could not fetch app version.',
        position: 'bottom',
        duration: 3000
      }).present()
    })
  }


  twende(item) {
    this.state = 1;
    this.autocomplete.query = item.destination;
    this.destination = item.cord;
    console.log('kenya', item.cord);
    this.isDestinationSet = true;

    this.displayRoute(this.currentLocation, item.cord, this.directionsService,
      this.directionsDisplay);
    this.fecthJourneyData()
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Your ride is on the way!',
      duration: 2000,
      position: 'top'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isRequesting = false;
      this.navCtrl.setRoot('CabEnroutePage')
    });
    toast.present();
  }
  ShuffleRecentplaces(a) {
    console.log('shuffle');
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
    this.recent = a;
    return a;
  }
}
