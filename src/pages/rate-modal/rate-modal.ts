import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the RateModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rate-modal',
  templateUrl: 'rate-modal.html',
})
export class RateModalPage {

  comment: any;
  rate: any;
  response: any;

  constructor(
    public viewCtrl: ViewController,
    public alert: AlertController,
    private rest: RestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private navCtrl: NavController) {

  }

  submit() {
    if (this.rate == null) {
      this.alert.create({
        title: 'ATTENTION!',
        message: 'Please give your rating by clicking on at least one star above.',
        buttons: ['Dismiss'],
        mode: 'ios'
      }).present()
    }
    else {
      let rateData = {
        "target_user_id": this.rest.driverID,
        "score": this.rate,
        "description": this.comment
      }
      let loading = this.loadingCtrl.create({
        content: 'Processing...',
        spinner: 'bubbles'
      })

      loading.present()

      this.rest.mainPost('/rate-individual', rateData).then((res) => {

        this.response = res
        loading.dismiss()
        console.log(this.response)

        if (this.response.status == 200) {
          this.navCtrl.setRoot('HomePage')
          let toast = this.toastCtrl.create({
            message: this.response.message,
            duration: 3000,
            position: 'top'
          });
        
          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });
        
          toast.present();
        }
        else {
          this.actionedAlert('Error', 'The operation could not be completed. Please try again.')
        }
      }, error => {
        loading.dismiss();
        console.log(error);
        this.actionedAlert('Error', 'The operation could not be completed. Please try again.')
      });
    }
  }

  onModelChange(rate) {
    this.rate = rate
    console.log("star event", this.rate)
  }

  actionedAlert(title, message) {
    this.alertCtrl.create({
      title: title,
      message: message,
      mode: 'ios',
      buttons: [{
        text: 'Cancel',
        handler: () => {
          this.navCtrl.setRoot('HomePage');
        }
      }, {
        text: 'Retry',
        handler: () => {
          this.submit();
        }
      }]
    }).present()
  }

  exitPage() {
    this.navCtrl.setRoot('HomePage')
  }
}
