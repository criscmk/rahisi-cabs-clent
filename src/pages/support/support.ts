import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage({
	name: 'page-support',
	segment: 'support'
})

@Component({
    selector: 'page-support',
    templateUrl: 'support.html'
})
export class SupportPage {

  constructor(public navCtrl: NavController,
    private callNumber: CallNumber) {

  }

  openChat() {
  	// console.log(proptype);
  	this.navCtrl.push('page-chat-detail');
  }
  call()
  {
    this.callNumber.callNumber("0706559254", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
