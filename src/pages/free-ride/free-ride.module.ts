import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FreeRidePage } from './free-ride';

@NgModule({
  declarations: [
    FreeRidePage,
  ],
  imports: [
    IonicPageModule.forChild(FreeRidePage),
  ],
})
export class FreeRidePageModule {}
