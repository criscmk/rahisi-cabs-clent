import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';



@IonicPage({
  name: 'page-my-account',
  segment: 'myaccount'
})

@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html'
})

export class MyAccountPage {
  profiledata: Boolean = true;
  profile: any;
  fname:any;
  lname: any;
  phone: any;
  address: any;
  email: any;
  prof={
    'fname':this.fname,
    lname:this.lname,
    email:this.email,
    phone:this.phone,
    address:this.address
  }
  id: any;
  matokeo: any;
 

  constructor(public navCtrl: NavController,
    public restProvider: RestProvider,
    public loadingCtrl: LoadingController, 
    
    public alertCtrl:AlertController,
    public toastCtrl: ToastController,
    public storage:Storage) {
      // this.getProfile();

  }

  // process send button
  sendData() {
    // send booking info
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    // show message
    let toast = this.toastCtrl.create({
      showCloseButton: true,
      cssClass: 'profiles-bg',
      message: 'Your Data was Edited!',
      duration: 3000,
      position: 'bottom'
    });

    loader.present();

    setTimeout(() => {
      loader.dismiss();
      toast.present();
      // back to home page
      this.navCtrl.setRoot('page-home');
    }, 3000)
  }
  getProfile() {

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })

    loading.present()
    this.restProvider.mainGet('user').
      subscribe(res => {
        loading.dismiss();
        this.profile = res;
        console.log(' profile', this.profile);
        this.id = this.profile.id
               this.storage.set('profile', this.profile.data);
        this.restProvider.email = this.profile.email;
        // this.restProvider.role = this.profile.role_id;
        // this.restProvider.userDetails = this.profile
        this.restProvider.name = this.profile.name;
        // this.restProvider.lname = this.profile.lname;
        // this.restProvider.phone = this.profile.phone;
        // this.restProvider.address = this.profile.address;

        this.storage.set('profile', this.profile);
       
	
      }, error => {
        loading.dismiss();
        console.log("Error", error)
      });

  }

update()
{
  console.log('data to be updated',this.prof);
  let loading = this.loadingCtrl.create({
    content: 'Updating...',
    spinner: 'bubbles'
  })
  //Change network status
  loading.present()
  this.restProvider.mainPost('update/'+this.id,this.prof).then((result) => {
    loading.dismiss();
    console.log(result);
    this.matokeo  = result
 
    if(this.matokeo.status==200)
    {
      let toast = this.toastCtrl.create({
        showCloseButton: true,
        cssClass: 'profiles-bg',
        message: 'Your Profile Updsated successfully!',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    
    }
    

    
    
    
  }, error => {
    loading.dismiss()
    console.log("error: ", error)
    this.alertCtrl.create({
      title: ' Error',
      message: 'Something went wrong',
      mode: 'ios',
      buttons: [{
        text: 'Dismiss',
        handler: () => {

        }
      }]
    }).present()
  });

}
}
