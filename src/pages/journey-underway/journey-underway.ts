import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController, ModalController, ToastController } from 'ionic-angular';
import * as firebase from 'firebase';
import { RestProvider } from '../../providers/rest/rest';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';

declare var google;
/**
 * Generated class for the JourneyUnderwayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-journey-underway',
  templateUrl: 'journey-underway.html',
})
export class JourneyUnderwayPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  directionsService: any;
  directionsDisplay: any;

  currentLocation: any = { lat: 0.0236, lng: 37.9062 };
  userMarker: any;

  startLocation: any = { lat: 0.0236, lng: 37.9062 };
  destination: any = { lat: 0.0236, lng: 37.9062 };

  distanceRemaining: number = 0;
  timeRemaining: number = 0;

  watch: any;
  driverPhoneNumber: string = "0711984124";

  constructor(public navCtrl: NavController, public navParams: NavParams, private zone: NgZone,
    private geolocation: Geolocation, private platform: Platform, private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, private restProvider: RestProvider, private modalCtrl: ModalController,
    private toastCtrl: ToastController, private callNumber: CallNumber) {

    // retrieve the starting point
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/startLocation').on('value', snapShot => {
      this.startLocation = snapShot.val()
      restProvider.startLocation = this.startLocation
    });

    // retrieve the destination
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/destination').on('value', snapShot => {
      this.destination = snapShot.val()
      restProvider.destination = this.destination
    });

    // retrieve the remaining distance
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/distanceRemaining').on('value', snapShot => {
      this.distanceRemaining = snapShot.val()
    });

    // retrieve the remaining time
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/timeRemaining').on('value', snapShot => {
      this.timeRemaining = snapShot.val()
    });

    // watch the ride status then navigate the user accordingly
    firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').on('value', snapShot => {

      let rideStatus = snapShot.val()

      console.log("journey status: ", rideStatus)

      if (rideStatus === 'completed') {
        firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').off()
        // journey has been started, navigate the user to ride summary modal
        navCtrl.setRoot('RideInvoiceModalPage', )
      } else if (rideStatus === 'cancelled') {
        firebase.database().ref('underwayRides/' + restProvider.user_details.phone + '/rideStatus').off()
        navCtrl.setRoot('HomePage')
      } else {
        return
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JourneyUnderwayPage');

    this.getUserLocation()
  }

  getUserLocation() {
    let loading = this.loadingCtrl.create({
      content: 'Just a moment...',
      spinner: 'crescent'
    })

    loading.present()

    this.platform.ready().then(() => {

      this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((resp) => {
        this.currentLocation = { lat: resp.coords.latitude, lng: resp.coords.longitude }
        console.log("My location: ", this.currentLocation)

        this.initializeMap()
        this.map.setCenter(this.currentLocation)
        this.userMarker.setPosition(this.currentLocation)

        loading.dismiss()

      }).catch((error) => {
        loading.dismiss()
        console.log('Error getting location', error);
      });

    });
  }

  watchLocation() {
    let options = {
      frequency: 15000,
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options).subscribe(position => {
      if ((position as Geoposition).coords != undefined) {
        var geoposition = (position as Geoposition);

        // Run update inside of Angular's zone
        this.zone.run(() => {
          this.currentLocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }

          this.userMarker.setPosition(this.currentLocation)
          this.map.setCenter(this.currentLocation)

        });

      } else {

        console.log('Error retrieving location')
      }
    });
  }

  initializeMap() {

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: this.currentLocation,
      disableDefaultUI: true,
      zoom: 14
    });

    let infowindow = new google.maps.InfoWindow({
      content: "My Location"
    });

    let icon = {
      url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png", // url
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    };

    this.userMarker = new google.maps.Marker({
      position: this.currentLocation,
      map: this.map,
      animation: google.maps.Animation.DROP,
      icon: icon,
    });

    // add a click listener to show infoWindow
    this.userMarker.addListener('click', function () {
      infowindow.open(this.map, this.userMarker);
    });

    // initialize the maps services
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      map: this.map,
    });

    // this.displayRoute(this.startLocation, this.destination, this.directionsService,
    //   this.directionsDisplay);

      this.watchLocation()

  }

  displayRoute(origin, destination, service, display) {
    service.route({
      origin: origin,
      destination: destination,
      travelMode: 'DRIVING',
      avoidTolls: true
    }, (response, status) => {
      if (status === 'OK') {
        display.setDirections(response);
      } else {
        alert('Could not display directions due to: ' + status);
      }
    });
  }

  callEmergency() {
    this.callNumber.callNumber("0711984124", true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
    // this.toastCtrl.create({
    //   message: 'Coming Soon.',
    //   position: 'bottom',
    //   duration: 3000
    // }).present()
  }

  shareRide() {
    this.toastCtrl.create({
      message: 'Coming Soon.',
      position: 'bottom',
      duration: 3000
    }).present()
  }

}
