import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JourneyUnderwayPage } from './journey-underway';

@NgModule({
  declarations: [
    JourneyUnderwayPage,
  ],
  imports: [
    IonicPageModule.forChild(JourneyUnderwayPage),
  ],
})
export class JourneyUnderwayPageModule {}
